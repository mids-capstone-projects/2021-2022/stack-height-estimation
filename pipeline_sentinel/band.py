##############################################################
# README:
# This script performs band stacking:
#   it stacks Sentinel 2 band B2, B3, and B4 to generate RGB image
#   
#
# Expectation:
#   - A directory that stored Sentinel 2 images
#   - the metadata JSON file stored under this directory
#   Note:
#           in this directory, images of every stack base should be stored in seperate folders
#           folder names of each stack base are the same as in meta.keys
##############################################################

from imgProcessing import *
from argparse import ArgumentParser

'''
Specify the name of the directory stored Sentinel 2 images
'''
parser = ArgumentParser()
parser.add_argument(
    "-i",
    "--data_dir",
    help = "name of the directory stored Sentinel 2 images",
    default = "data",
    type = str,
)
args = parser.parse_args()
# load metadata
with open("{}/meta".format(args.data_dir), "rb") as f:
    meta_data = pickle.load(f)
'''
Get all stack base id
meta data is a json file and all stack base id are keys
'''
stack_ids = meta_data.keys()
data = []
for stack_id in stack_ids:
    # Loop through the images for each stack base
    # get image names for each stack base
    img_names = get_all_img_names(meta_data, stack_id)
    # stack B2, B3, B4 to get RGB images
    perform_band_stacking(img_names, "{}/{}".format(args.data_dir, stack_id))
