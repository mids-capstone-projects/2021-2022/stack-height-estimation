import warnings
warnings.filterwarnings("ignore")
import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os
import ruptures as rpt
from argparse import ArgumentParser

from imgProcessing import *
from helper import *
from height_estimation import *
from measure_sl_1d import *

'''
Specify the name of the directory stored Sentinel 2 images
'''
parser = ArgumentParser()
parser.add_argument(
    "-i",
    "--data_dir",
    help = "name of the directory stored Sentinel 2 images",
    default = "data",
    type = str,
)
args = parser.parse_args()


# specify the method the pipeline is using
method = 'cpd_'
data_dir = args.data_dir
# output for each stack base would be stored in results/{output}/
output = method + data_dir
# load Metadata
with open("{}/meta".format(data_dir), "rb") as f:
    meta_data = pickle.load(f)
# get all id for stack bases
stack_ids = meta_data.keys()
# a list to store detailed results
data = []

# loop through stack bases
for im, stack_id in enumerate(stack_ids):
    img_names = get_all_img_names(meta_data, stack_id)
    print('Processing:', stack_id)

    # loop through images for each stack base
    for image_name in img_names:

        # extract date, mean azimuth angle, mean zenith angle, cloud coverage from meta data
        date = image_name[:8]
        az_angle = meta_data['{}'.format(stack_id)]['{}'.format(image_name)]['properties']['MEAN_SOLAR_AZIMUTH_ANGLE']
        zn_angle = meta_data['{}'.format(stack_id)]['{}'.format(image_name)]['properties']['MEAN_SOLAR_ZENITH_ANGLE']
        coverage = meta_data['{}'.format(stack_id)]['{}'.format(image_name)]['properties']['CLOUDY_PIXEL_PERCENTAGE']

        # create results directory to store each stack's associated output images if doesn't exist
        results_dir = 'results/{}/{}/{}'.format(output, stack_id, image_name)
        if os.path.exists(results_dir):
            pass
        else:
            os.makedirs(results_dir)

        # read in tif image, convert to numpy array and plot the RGB image
        with rasterio.open('{}/{}/{}.tif'.format(data_dir, stack_id, image_name)) as ds:
            img_arr = ds.read()
        img_arr = np.moveaxis(img_arr, 0, -1)/4095  # 12 bits image, 2^12 - 1 = 4095
        dir = '{}/{}.png'.format(results_dir, image_name + '_RGB')
        save_png(img_arr, dir)

        # crop the image and plot the cropped image
        # quad_id is the quadrant where shadow falls
        # top left - 0, top right - 1, bottom right - 2, bottom left - 3
        quad_id, cropped = crop_image(img_arr, az_angle)
        dir = '{}/{}.png'.format(results_dir, image_name + '_RGB_CROPPED')
        plot_rgb_cropped(cropped, dir)
        
        width = cropped.shape[1]
        height = cropped.shape[0]
        # array of all 4 possible coordinates (x, y) of the corner of stack base
        origins = [(width - 1, height - 1), 
            (0, height - 1), (0, 0), (width - 1, 0)]
        # get the coordinates of stack base based on current quadrant
        origin = origins[quad_id]

        # convert to grayscale and increase contrast
        gray_cropped = convert_to_greyscale(cropped)
        gray_cropped = adjust_contrast(gray_cropped)

        # calculate slope & intercept, plot the expected shadow line
        intercept, slope = linear_graph(quad_id, origin, az_angle)
        dir = '{}/{}.png'.format(results_dir, image_name + '_RGB_EXP_SHADOW')
        plot(origin, cropped, slope, dir)

        ###################
        #  1d estimation  #
        ###################

        # find 1d pixels on shadow line and plot
        on_line, intensity_values = find_1dpixels(quad_id, gray_cropped, slope, intercept)
        dir = '{}/{}.png'.format(results_dir, image_name + '_GRAY_1DLINE')
        plot_1dpixels(origin, gray_cropped, slope, intensity_values, dir)
        values = np.array(list(intensity_values.values()))

        # apply change point detection method with l2 penality to find change points
        algo = rpt.Dynp(model = "l2", jump = 1).fit(values)
        # set the number of change point (n_bkps) to be 3
        points = np.asarray(algo.predict(n_bkps = 3)) - 1

        shadow_stop = 0
        for p in points:
            if 0 < p and p < len(values) - 1:
                # identify the first change point with positive slope as the end of shadow
                if values[p + 1] - values[p - 1] > 0:
                    shadow_stop = p
                    break

        # plot the predicted shadow and the curve of intensity values along the line with identified change points
        dir_cp = '{}/{}.png'.format(results_dir, image_name + '_CP')
        dir_predshadow = '{}/{}.png'.format(results_dir, image_name + '_GRAY_PRED_SHADOW')
        plot_predicted_shadow(cropped, intensity_values, shadow_stop, dir_predshadow)
        plot_curve(intensity_values, origin, shadow_stop, points, dir_cp)

        # calculate shadow length (meter)
        pos_key = list(intensity_values.keys())[int(shadow_stop)]
        shadow_len = math.sqrt((origin[0] - pos_key[1]) ** 2 + (origin[1] - pos_key[0]) ** 2) * 10

        # calculate the prediction of stack height (feet)
        stack_height = calculate_stackheight(shadow_len, zn_angle)
        data.append([image_name, stack_id, date, coverage, zn_angle, az_angle, slope, intercept, shadow_len, stack_height])

# save the list of detailed information of each image
df = pd.DataFrame(data, columns = ['image_name', 'stack_base_id', 'date', 'cloud_coverage', 'zenith_angle', 'azimuth_angle', 'slope', 'intercept', 'shadow_length', 'stack_height_feet'])
df.to_csv('results/{}.csv'.format(output), index = False)

# save the list ensemble predictions of each stack
median = df.groupby(by = 'stack_base_id').median()['stack_height_feet'].reset_index()
median.to_csv('results/{}_prediction.csv'.format(output), index = False)