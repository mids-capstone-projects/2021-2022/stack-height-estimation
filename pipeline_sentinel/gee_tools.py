"""
CLI tool to download satellite imagery from google earth engine
"""

import ee
import numpy as np
import pandas as pd
import pickle
import os
import rasterio
from osm_tools import _bbox_from_point, gdf_from_bbox

ee.Initialize()

from skimage.io import imread

from geetools import batch
from tqdm import tqdm

from argparse import ArgumentParser
from config import *


def osm2gee_bbox(bbox):
    """
    convert bbox from lat lon (OSM default) to lon lat (GEE default)
    """
    return bbox[1], bbox[0], bbox[3], bbox[2]


def download_sen2_toLocal(bbox, dir, id, meta_dict, lat):
    """
    downloads Sentinel-2 imagery from the specified bounding box and saves it in dir/id/ folder
    """
    AOI = ee.Geometry.Rectangle(list(bbox), "EPSG:4326", False)
    fname = (
                f"{dir}/{id}"
            )
    # If lat > 0, north hemispheres, choose half of the year when shadow are longer
    if lat > 0:
        start_date = north_start_date  # 2020-10-01
        end_date = north_end_date  # 2021-03-31
    # If lat < 0, south hemispheres, choose half of the year when shadow are longer
    else:
        start_date = south_start_date  # 2020-04-01
        end_date = south_end_date  # 2020-09-30     

    collection = (
        ee.ImageCollection("COPERNICUS/S2_SR")
        .filterDate(start_date, end_date)
        # filter the cloud coverage < 5%
        .filter(ee.Filter.lt('CLOUDY_PIXEL_PERCENTAGE',cloud_percent))
        .filterBounds(AOI)
        # select R, G, B, Near-infrared band
        .select(['B2', 'B3', 'B4', 'B8'])
    )


    export_image(collection, dir, id, meta_dict, region=AOI)
    os.remove(f"{fname}.zip")

def export_image(collection, dir, id, meta_dict, scale=10, region=None):
    """ Batch export images to local directory, one image at a time
    Arguments
        :param collection: earth engine image collection object, a stack of images to export
        :param dir: specify the name of the folder to store images in
        :param id: specify the id for each stack base
        :param scale=10: resolution of the image. Set to the orginal resolution of Sentinel 2 by default
        :param region=None: Area of interest to crop the image to.
                            If pass in coordinates, the image will be cropped to
                            interested range of coordinates
    """

    colList = collection.toList(collection.size())
    n = collection.size().getInfo()
    meta_dict[id] = {}
    for i in range(n):
        img = ee.Image(colList.get(i))
        if region is None:
            region = img.geometry().bounds().getInfo()["coordinates"]
        batch.image.toLocal(
            image=img,
            name=f"{dir}/{id}",
            region=region,
            scale=scale
            )
        meta = img.getInfo()
        meta_dict[id][img.getInfo()['id'][17:]] = meta

if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("-i", "--input", help="path to csv input file", type=str)
    parser.add_argument(
        "-d",
        "--distance",
        help="side length for area of interest",
        default=1000,
        type=int,
    )
    parser.add_argument(
        "-o", "--output_dir", help="path to output directory", default="data/", type=str
    )
    parser.add_argument(
        "-lat",
        "--lat_col",
        help="name of the column that contains the latitude",
        default="lat",
        type=str,
    )
    parser.add_argument(
        "-lon",
        "--lon_col",
        help="name of the column that contains the longitude",
        default="lon",
        type=str,
    )
    parser.add_argument(
        "-id",
        "--id_col",
        help="name of the column that contains the unique id for each stack base location",
        default="stack_base_id",
        type=str,
    )

    args = parser.parse_args()
    if not os.path.exists(args.output_dir):
        os.makedirs(args.output_dir)
        pass

    points = pd.read_csv(args.input)
    points = points.dropna(subset=[args.id_col])

    logf = open(f"{args.output_dir}/error.log", "w")
    meta_dict = {}
    for id in tqdm(points[args.id_col].unique()):
        tmp = points[points[args.id_col] == id]
        for i, point in tqdm(tmp.iterrows()):
            fname = (
                f"{args.output_dir}/{point[args.id_col]}"
            )
            if os.path.exists(f"{fname}"):
                continue
            bbox = _bbox_from_point(
                (point[args.lat_col], point[args.lon_col]), args.distance
            )
            bbox = osm2gee_bbox(bbox)
            try:
                download_sen2_toLocal(bbox, args.output_dir, point[args.id_col], meta_dict, point[args.lat_col])
            except Exception as e:
                logf.write(f"stack base id {point[args.id_col]}: {e}\n")
                pass
            # save the metadata file to output_dir/meta
            with open(f"{args.output_dir}/meta", "wb") as f:
                pickle.dump(meta_dict, f)
                pass
