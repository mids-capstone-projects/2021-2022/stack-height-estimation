##############################################################
# README:
# This script contains the common functions for plotting
##############################################################

from scipy.optimize import curve_fit
import math
import matplotlib.pyplot as plt
plt.switch_backend('agg')
import numpy as np
from PIL import Image
import math
from config import *


'''
Calculate intercept and slope of the projected shadow line
based on azimuth angle and the quadrant the shadow falls

:param quad_id: ID indicating in which quadrant the shadow falls
:param origin: coordinate (x, y) of the pixel of the stack base (one of the four corners depending on quadrant)
:param az_angle: azimuth angle of the image
'''
def linear_graph(quad_id, origin, az_angle):
    shadow_angle = math.radians(90 - abs(180 - az_angle))
    slope = math.tan(shadow_angle)
    intercept = origin[1] - origin[0] * slope
    # slope need to be flipped for two quadrants
    if quad_id == 1 or quad_id == 2:
        slope = - slope
    return intercept, slope


'''
Plot the projected shadow line on the satellite imagery

:param origin: coordinate (x, y) of the pixel of the stack base (one of the four corners depending on quadrant)
:param img_arr: numpy array of satellite image
:param slope: slope of projected shadow line
:param dir: path to save the plot
'''
def plot(origin, img_array, slope, dir):
    x = origin[0]
    y = origin[1]
    fig, ax = plt.subplots(num = 'main', figsize = fig_size)
    plt.axis('off')
    plt.imshow(img_array, aspect = 'auto', cmap ='gray')
    # plot the pixel of stack base
    plt.scatter(x, y, c='red', s=30, label = 'Stack Base')
    # plot the projected shadow line
    plt.axline((x, y), slope = slope, color = 'red',
                label='Shadow Trajectory')
    # plot dashed lines to give a range around the shadow line
    plt.axline((x, y), slope = math.tan(np.arctan(slope) + math.radians(5)), color = 'red',
                linestyle = 'dashed')
    plt.axline((x, y), slope = math.tan(np.arctan(slope) - math.radians(5)), color = 'red',
                linestyle = 'dashed')
    plt.legend(fontsize = 18)
    plt.savefig(dir, transparent = True)
    plt.close()



'''
Plot the projected shadow line with pixels on the line

:param origin: coordinate (x, y) of the pixel of the stack base (one of the four corners depending on quadrant)
:param img_arr: numpy array of satellite image
:param slope: slope of projected shadow line
:param intensity_values: a dictionary for pixels on line
                         keys：coordinates (y, x) of pixels, values: intensity values of pixels
:param dir: path to save the plot
'''
def plot_1dpixels(origin, img_arr, slope, intensity_values, dir):
    x = origin[0]
    y = origin[1]
    fig, ax = plt.subplots(num = 'main', figsize = fig_size)
    plt.axis('off')
    plt.imshow(img_arr, aspect = 'auto', cmap = 'gray')
    # plot the line
    plt.axline((x, y), slope = slope, color = 'red',
                label = 'Shadow Trajectory')
    # plot the pixels
    for el in list(intensity_values.keys()):
        plt.scatter(x = el[1],y = el[0],c = 'yellow', s = 15)
    plt.legend(fontsize = 18)
    plt.savefig(dir, transparent = True)
    plt.close()



'''
Save the png in given directory

:param img_arr: numpy array of satellite image
:param dir: path to save png
'''
def save_png(img_array, dir):
    arr = (img_array / np.max(img_array) * 255).astype(np.uint8)
    png = Image.fromarray(arr)
    png.save(dir)



'''
Plot the cropped rgb image

:param img_arr: numpy array of satellite image
:param dir: path to save ong
'''
def plot_rgb_cropped(img_array, dir):
    fig, ax = plt.subplots(num = 'main', figsize=fig_size)
    plt.axis('off')
    plt.imshow(img_array, aspect = 'auto')
    plt.savefig(dir, transparent = True)
    plt.close()


'''
Plot the pixels which are predicted as shadow pixels

:param img_arr: numpy array of satellite image
:param intensity_values: a dictionary for pixels on line
                         keys：coordinates (y, x) of pixels, values: intensity values of pixels
:param shadow_stop: index of the pixel indentified as the end of shadow
:param dir: path to save ong
'''
def plot_predicted_shadow(img_arr, intensity_values, shadow_stop, dir):
    fig, ax = plt.subplots(num = 'main', figsize = fig_size)
    plt.axis('off')
    plt.imshow(img_arr, cmap='gray')
    for i, el in enumerate(list(intensity_values.keys())):
        plt.scatter(x = el[1],y = el[0],c = 'lawngreen', marker = 's', label = 'Predicted Shadow', s = 30)
        if i == int(shadow_stop):
            break
    plt.savefig(dir, transparent = True)
    plt.close()


'''
Plot the curve of intensity values vs. pixel index on line with identified change points

:param intensity_values: a dictionary for pixels on line
                         keys：coordinates (y, x) of pixels, values: intensity values of pixels
:param origin: coordinate (x, y) of the pixel of the stack base (one of the four corners depending on quadrant)
:param shadow_stop: index of the pixel indentified as the end of shadow
:param points: 
:param dir: path to save ong
'''
def plot_curve(intensity_values, origin, shadow_stop, points, dir):
    coord = list(intensity_values.keys())
    values = list(intensity_values.values())
    l = len(coord)
    lens = [None] * l
    # calculate the corresponding distance between pixels and the stack base (origin) as an axis
    for i in range(l):
        lens[i] = math.sqrt((origin[0] - coord[i][1]) ** 2 + (origin[1] - coord[i][0]) ** 2) * 10 * 3.28084
    fig, ax = plt.subplots(num = 'main', figsize = cp_figsize)
    plt.plot(values, color = 'black')
    for i in points:
        # plot the red vertical line of the point identified as end of the shadow
        if i == shadow_stop:
            plt.axvline(x = i, color = 'red')
        else:
            plt.axvline(x = i)
    plt.title('Intensity Values vs. Pixel Index on Line')
    plt.xlabel('Pixel Index on Line')
    plt.ylabel('Intensity Value')
    ax2 = ax.twiny()
    ax2.set_xlabel("distance (feet)")
    ax2.set_xticks(np.arange(0, l, 5))
    ax2.set_xbound(ax.get_xbound())
    ax2.set_xticklabels([int(lens[int(x)]) for x in np.arange(0, l, 5)])
    plt.savefig(dir, transparent = True)
    plt.close()