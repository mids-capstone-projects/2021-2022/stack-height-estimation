'''
config file used to store variables
'''

##### gee_tools.py #####
# date range to download images
north_start_date = "2020-10-01"
north_end_date = "2021-03-31"

south_start_date = "2020-04-01"
south_end_date = "2020-09-30"

# cloud coverage filter
cloud_percent = 5


##### helper.py #####
fig_size = (10, 10)  # satelliet image size
cp_figsize = (10, 8)  # change point plot size


##### imgProcessing.py #####
dark_thresh = 0.30
intensify_ind = 1.2
grey_weights = [0.2989, 0.5870, 0.1140]