##############################################################
# This script calculates stack height from shadow 
##############################################################

import math
import helper


'''
Calculate stack height (feet)

:param shadow_length: shadow length (meter)
:param zn_angle: sun zenith angle from metadata
'''
def calculate_stackheight(shadow_length, zn_angle):
    sl_ft = shadow_length * 3.28084  # 1 meter = 3.28084 feet
    # sun elevation angle by definition:
    elevation = 90 - zn_angle
    # sun elevation radius angle
    elevation_rad = math.radians(elevation)
    # calculate stack height with trigonometric
    height = math.tan(elevation_rad) * sl_ft

    return height
