# Project overview

Our aim with this project was to estimate the heights of power plant stacks from publicly available satellite imagery data. Our client, WattTime, is working to build the first automated system for monitoring the emission from power plants globally, utilizing the height of flue-gas stacks to better understand the dispersal of pollutants as well as the contribution of power plants to climate change. We estimated the length of the shadow cast by the flue stack and use physical information to obtain the estimations of stack heights. We have applied different methods to detect the region of shadow pixels, among which change point detection is the chosen method with the best performance. We've developed the pipeline of change point detection to predict stack heights and applied the pipeline to create a database with over 16,000 global stack heights.

# Database

We've applied our pipeline to generate a database of stack heights (feet) with 16,260 global stack bases.

- [Global Stack Height Database](/stack_height_database.csv)

# Docs

- [Final Paper](documents/Sizing_the_Stack_-_Capstone_Symposium.pdf)
- [Final Slides](documents/Sizing_the_Stack_-_Capstone_Symposium.pdf)
- [Slides for past meetings](https://drive.google.com/drive/folders/1AC7iXdc6C-13FuEZT3E6QOfxhZdSOLE9)

# Pipeline

Here is the tutorial on how to use our pipeline to estimate stack heights (feet) using Sentinel 2/ Planet 3 satellite imagery. Our pipeline applies the change point detection method for estimation. 

## Installation

First, ```git clone``` this repository. 

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install all dependent pakages.

```bash
pip install -r requirements.txt
```


## General Workflow to use the Sentinel 2 pipeline:

1. Use ```gee_tools.py``` to download Sentinel 2 images from Google Earth Engine API:
    
    run ```python pipeline_sentinel/gee_tools.py -i coordinates/sample.csv```
    
    This step results in a ```data/``` folder which contains ```.tif``` file of every band of every image.
    
2. Use ```band.py``` to combine R, G, B bands:

    run ```python pipeline_sentinel/band.py```
    
    This step results in a ```.tif``` file of RGB of every image

3. Use ```pipeline.py``` to perform stack height estimation:
    
    run ```python pipeline_sentinel/pipeline.py```
    
    This step results in a ```results/cpd_data/``` directory which contains processed images and change point plots, and csv file of predictions of stack heights (feet). (For check, ```results/cpd_data_prediction.csv``` is the results for sample coordinates)

4. If you have ground truth of stack heights, use ```metrics.py``` to generate a csv file of performance metrics of predictions and plots of errors.

    run ```python pipeline_sentinel/metrics.py```


## General Workflow to use the Planet 3 pipeline:

1. Prepare a directory ```planet3_data``` with tif files of Planet3 images and a csv file ```image-metadata.csv``` of metadata. ```image-metadata.csv``` needs to contain columns of ```image_id```, ```lat```, ```lon```, ```cloud_cover```, ```sun_azimuth```, and ```sun_elevation```.

Here is an example of the required content of planet3 data directory:

<img src="https://user-images.githubusercontent.com/69910113/163876777-acbc7d5c-af7b-4beb-9e62-b58b065ebaea.png" alt="drawing" width="200"/>

2. Use ```pipeline.py``` to perform stack height estimation
    
    run ```python pipeline_planet/pipeline.py```
    
    This step results in a ```results/cpd_planet3_data/``` directory which contains processed images and change point plots, and csv file of estimations of stack heights (feet).

4. If you have ground truth of stack heights, use ```metrics.py``` to generate a csv file of performance metrics of predictions and plots of errors.

    run ```python pipeline_planet/metrics.py -i cpd_planet3_data```

 

## Documentation of scripts you may need to run:

**gee_tools.py**

To download the satellite imagery a google account that is registered to use the [Earth Engine API](https://developers.google.com/earth-engine) is needed. 

Once registered, run 

```bash
earthengine authenticate
``` 

in bash to verify your account.

Example use of gee_tools.py:

```bash
python gee_tools.py -i coordinates/sample.csv
```

This CLI tool can download imagery from the Sentinel-2 satellite. ```coordinates/sample.csv``` is one example of csv input file, which needs to contain columns of latitude, longtitude and id of stack bases.

Additional arguments:

```
usage: gee_tools.py [-h] [-i INPUT] [-d DISTANCE]
                    [-o OUTPUT_DIR] [-lat LAT_COL] [-lon LON_COL]
                    [-id ID_COL]

arguments:
  -h, --help          show this help message and exit
  -i INPUT, --input INPUT
    path to csv input file
  -d DISTANCE, --distance DISTANCE
    side length for area of interest (meter)
    Default = 1000
  -o OUTPUT_DIR, --output_dir OUTPUT_DIR
    path to output directory 
    Default = data/
  -lat LAT_COL, --lat_col LAT_COL
    name of the column that contains the latitude 
    Default = lat
  -lon LON_COL, --lon_col LON_COL
    name of the column that contains the longitude 
    Default = long
  -id ID_COL, --id_col ID_COL
    name of the column that contains the unique id for each stack base location
    Default = stack_base_id

```
(This tool is modified from [GEODOME](https://github.com/energydatalab/geodome))

**band.py**

This script performs band stacking for the Sentinel 2 pipeline:

   it stacks Sentinel 2 band B2, B3, and B4 to generate RGB image  

Expectation:
  - A directory that stores Sentinel 2 images
  - the metadata JSON file stored under this directory

Note:
        in the directory, images of every single stack should be stored in seperate folders
        folder names of each stack base are the same as in meta.keys

Example use:
```bash
python band.py -i data
arguments:
  -i INPUT, --input INPUT
    path to the directory that stored Sentinel 2 images
    Default = data
```

**pipeline.py**

This script applies change point detection method to imagery and obtains ensemble height estimation for each stack base.

The associate output imagery would be saved in ```results/cpd_data/``` directory. The detailed information of each image would be saved in ```results/cpd_data.csv```. The final predictions for all stack bases would be save in ```results/cpd_data_prediction.csv```.

Example use:
```bash
python pipeline.py -i data
arguments:
  -i INPUT, --input INPUT
    path to the directory that stored satellite images
    Default = data
```

**metrics.py** (Optional)

If you have ground truth of stack heights, this script analyzes the performance of the result of prediction.
```coordinates/ground_truth.csv``` is one example of csv file for ground truth, which needs to contain columns of ```stack_base_id``` and ```target_stack_height_feet```.

The overall metrics of ensembled predictions would be saved in ```results/overall_metrics_*.csv```. We also provide separate metrics of multiple predictions for each stack base, which would be saved in ```results/metrics_*.csv```.

Example use:
```bash
python metrics.py -i cpd_data
arguments:
  -i INPUT, --input INPUT
    name of the result csv file to analyze
    Default = cpd_data
```

## License and Author information

The tool is available under MIT license. See LICENSE for more information.

Author info:

Dean Huang (deanhuang.tc@gmail.com)

Nikhil Bhargava (Nikhil.atx.bhargava@gmail.com)

Xiaohan Yang (xyang0507@gmail.com)

Yijia Zhang (icy0177@gmail.com)
