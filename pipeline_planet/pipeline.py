import warnings
warnings.filterwarnings("ignore")
import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os
import ruptures as rpt
from collections import defaultdict
from argparse import ArgumentParser

from imgProcessing import *
from helper import *
from height_estimation import *
from measure_sl_1d import *

###################################################

'''
Specify the name of the directory stored Planet 3 images
'''
parser = ArgumentParser()
parser.add_argument(
    "-i",
    "--data_dir",
    help = "name of the directory stored Planet 3 images",
    default = "planet3_data",
    type = str,
)
args = parser.parse_args()
data_dir = args.data_dir

# Specify the method pipeline is using
method = 'cpd_'
# output for each stack base would be stored in results/{output}/
output = method + data_dir

# load metadata
meta_data = pd.read_csv(f"{data_dir}/image-metadata.csv", usecols = ['image_id', 
                                                            'lat', 'lon', 'sun_azimuth',
                                                            'sun_elevation', 'cloud_cover'])
img_dic = {}
for i in range(meta_data.shape[0]):
    stack_id = f'POINT({meta_data.iloc[i]["lon"]} {meta_data.iloc[i]["lat"]})'
    if stack_id not in img_dic:
        img_dic[stack_id] = {}
    img_dic[stack_id][meta_data.iloc[i]["image_id"]] = {'lat': meta_data.iloc[i]["lat"], 
            'lon': meta_data.iloc[i]["lon"],
            'sun_azimuth': meta_data.iloc[i]["sun_azimuth"],
            'sun_elevation': meta_data.iloc[i]["sun_elevation"],
            'cloud_cover': meta_data.iloc[i]["cloud_cover"]}
                 
'''
Recenter image - make stack base to be the center of image
'''
for stack_id, imgs in img_dic.items():
    for image_name in imgs:
        tif_path = f'{data_dir}/{image_name}.tif'
        # check if the image is there
        if os.path.isfile(tif_path):
            recentered_img_arr = recenter_img(
                data_dir, image_name, stack_id, imgs[image_name]['lat'], imgs[image_name]['lon'])
            img_arr = get_RGBbands(recentered_img_arr)
            # create results directory for each stack base and associated image (output images) if doesnt exist
            results_dir = 'results/{}/{}/{}'.format(output, stack_id, image_name)
            if not os.path.exists(results_dir):
                os.makedirs(results_dir)
            dir = f'{results_dir}/{image_name}_RGB_FULL.png'
            save_png(img_arr, dir)

data = []
# Loop through the images for each plant
for stack_id in img_dic:
    print('Processing:', stack_id)

    for image_name in img_dic[stack_id]:

        results_dir = 'results/{}/{}/{}'.format(output, stack_id, image_name)

        # Extract date, mean azimuth angle, mean zenith angle, and cloud coverage
        date = image_name[:8]
        az_angle = img_dic[stack_id][image_name]['sun_azimuth']
        zn_angle = 90 - img_dic[stack_id][image_name]['sun_elevation']
        coverage = img_dic[stack_id][image_name]['cloud_cover']

        # check if image exists
        rgb_full_path = f'{results_dir}/{image_name}_RGB_FULL.png'
        if not os.path.isfile(rgb_full_path):
            continue

        img_arr = plt.imread(rgb_full_path)

        # crop the image and plot the cropped image
        # quad_id is the quadrant where shadow falls
        # top left - 0, top right - 1, bottom right - 2, bottom left - 3        
        quad_id, cropped = crop_image(img_arr, az_angle)
        dir = '{}/{}.png'.format(results_dir, image_name + '_RGB_CROP')        

        plot_rgb_cropped(cropped, dir)
        # convert to grayscale
        gray_cropped = convert_to_greyscale(cropped)
        img_arr = gray_cropped

        width = img_arr.shape[1]
        height = img_arr.shape[0]
        # array of all 4 possible coordinates (x, y) of the corner of stack base
        origins = [(width - 1, height - 1), 
            (0, height - 1), (0, 0), (width - 1, 0)]
        # get the coordinates of stack base based on current quadrant
        origin = origins[quad_id]
        
        # Caluclation of slope & intercept, plot the expected shadow line
        intercept, slope = linear_graph(quad_id, origin, az_angle)
        # Output RGB image with bands
        dir = '{}/{}.png'.format(results_dir, image_name + '_RGB_EXP_SHADOW')        
        plot(origin, img_arr, slope, dir)

        ###################
        #  1d estimation  #
        ###################

        # find 1d pixels on shadow line and plot
        on_line, intensity_values = find_1dpixels(quad_id, img_arr, slope, intercept)
        dir = '{}/{}.png'.format(results_dir, image_name + '_GRAY_1DLINE')        
        plot_1dpixels(origin, img_arr, slope, intensity_values, dir)
        values = np.array(list(intensity_values.values()))

        # apply change point detection method with l2 penality to find change points
        algo = rpt.Dynp(model="l2", jump = 1).fit(values)
        # set the number of change point (n_bkps) to be 3
        points = np.asarray(algo.predict(n_bkps = 3)) - 1
        
        shadow_stop = 0
        for p in points:
            if 0 < p and p < len(values) - 1:
                if values[p + 1] - values[p - 1] > 0:
                    shadow_stop = p
                    break

        # plot the predicted shadow and the curve of intensity values along the line with identified change points
        dir_cp = '{}/{}.png'.format(results_dir, image_name + '_CP')
        dir_predshadow = '{}/{}.png'.format(results_dir, image_name + '_GRAY_PRED_SHADOW')
        plot_predicted_shadow(cropped, intensity_values, shadow_stop, dir_predshadow)
        plot_curve(intensity_values, origin, shadow_stop, points, dir_cp)
            
        # calculate shadow length (meter)
        pos_key = list(intensity_values.keys())[int(shadow_stop)]
        shadow_len = math.sqrt((origin[0] - pos_key[1]) ** 2 + (origin[1] - pos_key[0]) ** 2) * 10
        
        # calculate the prediction of stack height (feet)
        stack_height = calculate_stackheight(shadow_len, zn_angle)
        data.append([image_name, stack_id, date, coverage, zn_angle, az_angle, slope, intercept, shadow_len, stack_height])

# save the list of detailed information of each image
df = pd.DataFrame(data, columns = ['image_id', 'stack_base_id', 'date', 'cloud_coverage', 'zenith_angle', 'azimuth_angle', 'slope', 'intercept', 'shadow_length', 'stack_height_feet'])
df.to_csv('results/{}.csv'.format(output), index = False)

# save the list ensemble predictions of each stack
median = df.groupby(by = 'stack_base_id').median()['stack_height_feet'].reset_index()
median.to_csv('results/{}_prediction.csv'.format(output), index = False)