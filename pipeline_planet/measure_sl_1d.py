import numpy as np
import math


'''
Determine which pixels are on the projected shadow line

:param quad_id: ID indicating in which quadrant the shadow falls
:param img_arr: numpy array of satellite image
:param slope: slope of projected shadow line
:param intercept: intercept of projected shadow line in a coordinate plane
'''
def find_1dpixels(quad_id, img_arr, slope, intercept):
    x_range = img_arr.shape[1] - 1  # width of the image
    y_range = img_arr.shape[0] - 1  # height of the image

    '''
    Depending on in which quadrant shadow falls, projected shadow line would end at different image border
    - for top left/top right quadrants (quadrant 1&2): 
        shadow line ends at the top border of the image
        in other words, when y = 0
    - for bottom right/left quadrants (quadrant 3&4):
        shadow line ends at the bottom border of the image
        in other words, when y = y_range
    '''
    y_val_intercept = 0 if quad_id < 2 else y_range

    # calculate stop value (when line hits top of image)
    stop = round((y_val_intercept - intercept) / slope)

    # calculate num value (length of expected shadow angle, hypot of triangle)
    hypot = int(math.sqrt((y_range + 1) ** 2 + (x_range - stop) ** 2))

    # calculate linear space from start to stop value
    # multiple hypot by variable = 1.5 to increase size of linear space, prevent data loss
    if quad_id == 1 or quad_id == 2:
        if stop == 0:
            stop = stop + 1
        x_values = np.linspace(start = 0, stop = stop, num = int(hypot * 1.5))
    else:
        if stop == x_range:
            stop = stop - 1
        x_values = np.linspace(start = x_range, stop = stop, num = int(hypot * 1.5))
    
    # a dictionary to store pixels on line, keys: coordinates of pixels, values: corresponding intensity values
    # NOTE: coordinates are in (y, x) positon NOT (x, y)
    intensity_values = {}
    pixels = []
    for x_val in x_values:

        # store element value, element lower bound, and element upper bound
        cur_intensity_value_range = []

        # calculate x value on the line, set y_val as int
        y_val = int(round(slope * x_val + intercept))
        x_val = int(round(x_val))

        # append current intensity value
        if y_val < 0 or x_val < 0 or y_val > y_range or x_val > x_range:
            break
        else:
            # append coordinate
            pixels.append([y_val, x_val])
            cur_intensity_value_range.append(img_arr[y_val, x_val])

        # obtain lower bound value
        try:
            cur_intensity_value_range.append(img_arr[y_val + 1, x_val])
        except:
             cur_intensity_value_range.append(img_arr[y_val, x_val])

        # obtain upper bound value
        try:
            cur_intensity_value_range.append(img_arr[y_val - 1, x_val])
        except:
            cur_intensity_value_range.append(img_arr[y_val, x_val])

        # obtain pixel value to right
        try:
            cur_intensity_value_range.append(img_arr[y_val, x_val + 1])
        except IndexError:
            cur_intensity_value_range.append(img_arr[y_val, x_val])

        # obtain pixel value to left
        try:
            cur_intensity_value_range.append(img_arr[y_val, x_val - 1])
        except IndexError:
            cur_intensity_value_range.append(img_arr[y_val, x_val])

        intensity_values[(y_val, x_val)] = np.min(np.array(cur_intensity_value_range), axis = 0)

    return pixels, intensity_values