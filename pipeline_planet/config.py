'''
config file used to store variables
'''

##### helper.py #####
fig_size = (10, 10)  # satelliet image size
cp_figsize = (10, 8)  # change point plot size


##### imgProcessing.py #####
dark_thresh = 0.30
intensify_ind = 1.2
grey_weights = [0.2989, 0.5870, 0.1140]