##############################################################
# README:
# This python script performs image preprocessing, including:
# 	- image cropping
#   - grey scale
#	- shadow angle estimation
#
# The below functions are supposed be called sequentially.
##############################################################

import numpy as np
import rasterio
from osm_tools import _bbox_from_point
from shapely.geometry import Polygon
from rasterio.mask import mask

from config import *


'''
Recenter image so the center is the target stack base

:param data_dir: directory that stores Planet 3 images
:param img_name: string of image name
:param plant: string of power plant the image belongs
:param lat: latitude of stack base
:param lon: longitude of stack base
'''
def recenter_img(data_dir, img_name, plant, lat, lon):
	# distance in meter from center to side
	dist = 750
	bbox = _bbox_from_point((lat, lon), dist)
	with rasterio.open(f'{data_dir}/{img_name}.tif') as src:
		crs = str(src.meta['crs'])[-5:]
		geom = rasterio.warp.transform_geom({'init': 'EPSG:4326'}, 
                             {'init': 'EPSG:{}'.format(crs)}, 
                             Polygon(zip(
                                 [bbox[1], bbox[3], bbox[3], bbox[1]], 
                                 [bbox[0], bbox[0], bbox[2], bbox[2]])))
		recentered_image, _ = mask(src,[geom], nodata = 0, crop = True, all_touched = True)
	return recentered_image


'''
Get R, G, B bands from the 12 bands of Planet 3 image .tif file

:param img_arr: numpy array of satellite image
'''
def get_RGBbands(img_arr):
	img_arr = np.moveaxis(img_arr, 0, -1)/np.max(img_arr)
	# get rgb bands
	img_arr = img_arr[:, :, np.array([2, 1, 0])]
	return img_arr


'''
Note: Prior to calling this function, the image tif file should be load in as Numpy array using rasterio

Image cropping function:
- This function uses the angle of the shadow to determine which direction to crop the image.

:params image_arr: the numpy array of the original non-cropped image
:params az_angle: solar azmuth angle used to determine the angle of the shadow
:params crop_frac: the fraction of how much the cropped images takes up in the original image
'''
def crop_image(image_arr, az_angle, crop_frac = 0):
	img_height = image_arr.shape[0]
	img_width = image_arr.shape[1]

	# keep bottom left of the image
	if az_angle < 90:
		return 3, image_arr[int(img_height/2) : int(img_height*(1-crop_frac)), int(img_width*crop_frac) : int(img_width/2)]
	# keep top left of the image
	elif az_angle < 180:
		return 0, image_arr[int(img_height*crop_frac) : int(img_height/2), int(img_width*crop_frac):int(img_width/2)]
	# keep top right of the image
	elif az_angle < 270:
		return 1, image_arr[int(img_height*crop_frac) : int(img_height/2), int(img_width/2):int(img_width*(1-crop_frac))]
    # keep bottom right of the image
	else:
		return 2, image_arr[int(img_height/2) : int(img_height*(1-crop_frac)), int(img_width/2):int(img_width*(1-crop_frac))]

'''
Converting to greyscale function:
- This function reads in an image's numpy array and convert the image into greyscale using the weights

:params image_arr: the numpy array of the image to convert
:params weights: the weights applied to BGR bands. Set to [0.2989, 0.5870, 0.1140] by default
'''
def convert_to_greyscale(image_arr, weights=grey_weights):
	grey_image = np.average(image_arr, weights = weights, axis=2)
	return grey_image


'''
Adjusting image contrast:
- This function reads in an image's numpy array and increases image's contrast for better analysis

:params img_arr: the numpy array of the image
'''
def adjust_contrast(img_arr):
	# make darker pixels darker
	# defult dark_thresh = 0.30; intensify_ind = 1.2
	img_arr = np.where(img_arr <= dark_thresh, img_arr ** intensify_ind, img_arr)
	return img_arr
