import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from sklearn.metrics import mean_absolute_error, mean_squared_error, median_absolute_error
from argparse import ArgumentParser

'''
Specify the name of the result csv for analyze
'''
parser = ArgumentParser()
parser.add_argument(
    "-i",
    "--csv_name",
    help = "name of the result csv file for analyze",
    default = "cpd_planet3_data",
    type = str,
)
args = parser.parse_args()
csv_name = args.csv_name

# Read the csv file
df = pd.read_csv('results/' + csv_name + '.csv')
df_ensemble = pd.read_csv('results/' + csv_name + '_prediction.csv')
# Get the actual stack height
col_list = ['stack_base_id','target_stack_height_feet']
actual = pd.read_csv('coordinates/ground_truth.csv', usecols = col_list)
# Merge the actual stack height column to df
df = df.merge(actual, how = 'left', on = 'stack_base_id')
df_ensemble = df_ensemble.merge(actual, how = 'left', on = 'stack_base_id')
# Give an estimate by median if no results
n_empty = sum(df_ensemble['stack_height_feet'] == 0)
df_ensemble['stack_height_feet'].replace(0, 200, inplace = True)

###################### Overall ######################
# Calculate the overall rmse
rmse  = mean_squared_error(df_ensemble.target_stack_height_feet, df_ensemble.stack_height_feet) ** 0.5
# Calculate the overall mean absolute error
mae  = mean_absolute_error(df_ensemble.target_stack_height_feet, df_ensemble.stack_height_feet)
# Calculate the overall median absolute error
medae  = median_absolute_error(df_ensemble.target_stack_height_feet, df_ensemble.stack_height_feet)
# Calculate mean absolute percentage error
ape_mean = abs((df_ensemble['stack_height_feet'] - df_ensemble['target_stack_height_feet'])/ df_ensemble['target_stack_height_feet']*100).mean()
# Calculate median absolute percentage error
ape_median = abs((df_ensemble['stack_height_feet'] - df_ensemble['target_stack_height_feet'])/ df_ensemble['target_stack_height_feet']*100).median()
# Create a dictionary combining all the metrics
data = {'mean_absolute_percentage_error' : ape_mean.round(2),
        'median_absolute_percentage_error' : ape_median.round(2),
        'RMSE' : rmse.round(2),
        'MEAN_ABSOLUTE_ERROR' : mae.round(2),
        'MEDIAN_ABSOLUTE_ERROR' : medae.round(2),
        'Number of estimates given by median' : n_empty}
df_overall = pd.DataFrame(data, index = ['Overall Metrics'])
df_overall.to_csv('results/overall_metrics_{}.csv'.format(csv_name), index = True)

###################### By stack base ######################
# Calculate mean stack height for each stack base
mean = df.groupby(by = 'stack_base_id').mean()
# Calculate median stack height for each stack base
median = df.groupby(by = 'stack_base_id').median()
# Calculate RMSE for each stack base
rmse = df.groupby(by = 'stack_base_id').apply(lambda x: mean_squared_error(x.target_stack_height_feet, x.stack_height_feet)**0.5).values
# Calculate Mean Absolute Error for each stack base
mae = df.groupby(by = 'stack_base_id').apply(lambda x: mean_absolute_error(x.target_stack_height_feet, x.stack_height_feet)).values
# Calculate Median Absolute Error for each stack base
medae = df.groupby(by = 'stack_base_id').apply(lambda x: median_absolute_error(x.target_stack_height_feet, x.stack_height_feet)).values
# Calculate Absolute Percentage Error for mean stack height for each stack base
ape_mean = abs((mean['stack_height_feet'] - mean['target_stack_height_feet']) / mean['target_stack_height_feet'] * 100).values
# Calculate Absolute Percentage Error for median stack height for each stack base
ape_median = abs((median['stack_height_feet'] - median['target_stack_height_feet']) / median['target_stack_height_feet'] * 100).values
# Create a dictionary combining all the metrics
data = {'stack_base_id': mean.index.values,
        'mean_stack_height' : mean.stack_height_feet.values.round(2) ,
        'median_stack_height' : median.stack_height_feet.values.round(2) ,
        'target_stack_height_feet' : mean.target_stack_height_feet.values.round(2),
        'absolute_percentage_error (mean)' : ape_mean.round(2),
        'absolute_percentage_error (median)' : ape_median.round(2),
        'RMSE' : rmse.round(2),
        'MEAN_ABSOLUTE_ERROR' : mae.round(2),
        'MEDIAN_ABSOLUTE_ERROR' :medae.round(2)}
df_final = pd.DataFrame(data)
df_final.to_csv('results/metrics_{}.csv'.format(csv_name), index = True)